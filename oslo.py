import typer
import boto3
from botocore.exceptions import ClientError

s3 = boto3.resource('s3')
oslo_app = typer.Typer(help="Oslo is a garbage collector of the A-team CI pipeline")

s3_app = typer.Typer()
oslo_app.add_typer(s3_app, name="s3")

bucket_app = typer.Typer()
s3_app.add_typer(bucket_app, name="bucket")


@s3_app.command("print")
def print_bucket_names():
    for bucket in s3.buckets.all():
        typer.echo(f"Hello {bucket.name}")


@bucket_app.command("get-lifecycle")
def get_bucket_lifecycle_of_s3(bucket_name: str):
    session = boto3.session.Session()
    s3_client = session.client('s3')
    try:
        result = s3_client.get_bucket_lifecycle_configuration(Bucket=bucket_name)
    except ClientError as e:
        raise Exception("boto3 client error in get_bucket_lifecycle_of_s3 function: " + e.__str__())
    except Exception as e:
        raise Exception("Unexpected error in get_bucket_lifecycle_of_s3 function: " + e.__str__())
    typer.echo(result)
    return result


@bucket_app.command("set-lifecycle")
def set_bucket_lifecycle_of_s3(bucket_name: str, lifecycle: str):
    session = boto3.session.Session()
    s3_client = session.client('s3')
    try:
        result = s3_client.put_bucket_lifecycle_configuration(Bucket=bucket_name, LifecycleConfiguration=lifecycle)
    except ClientError as e:
        raise Exception("boto3 client error in get_bucket_lifecycle_of_s3 function: " + e.__str__())
    except Exception as e:
        raise Exception("Unexpected error in get_bucket_lifecycle_of_s3 function: " + e.__str__())
    typer.echo(result)
    return result


if __name__ == "__main__":
    oslo_app()
