FROM registry.centos.org/centos:8
ENV HOME="/home/tft" \
    PATH="/home/tft/.local/bin:$PATH"
LABEL maintainer="tft@redhat.com"
WORKDIR /opt/oslo
COPY . /opt/oslo

RUN useradd --create-home --shell /bin/bash tft \
    && echo "export PATH=\"/home/tft/.local/bin:/usr/local/bin:$PATH\"" > /etc/environment
RUN dnf -y install python3-setuptools python3-pip

USER tft

RUN pip3 install --upgrade pip setuptools --user
RUN pip3 install poetry --user
RUN poetry config virtualenvs.create false
RUN poetry install

ENTRYPOINT ["/usr/bin/oslp.py"]
CMD ["--help"]
